defmodule Extractor do
  def remove_chars(line) do
    Regex.scan(~r/\d+(\.\d+)?/, line)
    |> List.flatten()
    |> Enum.flat_map(&String.graphemes/1)
    |> Enum.map(&String.to_integer/1)
  end

  def combine_first_and_last([first | rest]) do
    case Enum.reverse(rest) do
      [last | _] ->
        double_digit_number = first * 10 + last
        IO.inspect(double_digit_number, label: "Result")
        double_digit_number
      _ ->
        double_digit_number = first * 10 + first
        IO.inspect(double_digit_number, label: "Result")
        double_digit_number
    end
  end
end

result =
  File.stream!("input-data.txt")
  |> Stream.map(&String.trim/1)
  |> Stream.map(&Extractor.remove_chars/1)
  |> Stream.map(&Extractor.combine_first_and_last/1)
  |> Enum.reduce(0, &(&1 + &2))

IO.puts("Total result: #{result}")

#result = Extractor.remove_chars(input)
#IO.inspect(result)

#IO.puts(content)
#numbers = Regex.scan(~r/\d+/, content)
#          |> Enum.map(&String.to_integer/1)
#IO.inspect(numbers)
