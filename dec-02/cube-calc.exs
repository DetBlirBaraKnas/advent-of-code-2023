defmodule CubeGame do
  @doc """
  Returns int of game-id

  ## Example

      iex> game = "Game 77: 7 blue, 9 red, 1 green; ... 5 red, 1 green"
      iex> CubeGame.get_game_id(game)
      77
  """
  def get_game_id(game) do
    [ game_id | _       ] = String.split(game, ":")
    [ _       | game_id ] = String.split(game_id, " ")
    String.to_integer(List.to_string(game_id))
  end

  @doc """
  Returns list of strings containg rounds given string with game

  ## Example

      iex> game = "Game 77: 7 blue, 9 red, 1 green; ... 5 red, 1 green"
      iex> CubeGame.get_rounds(game)
      [" 7 blue, 9 red, 1 green", " 8 green", " 10 green, 5 blue, 3 red",
      " 11 blue, 5 red, 1 green"]
  """
  def get_rounds(game) do
    [ _ | rounds ] = String.split(game, ":")
    rounds = String.split(List.to_string(rounds), ";")
  end

  @doc"""
  Returns map of cubes from given string

  ## Example

      iex> round = "9 green, 9 red, 3 blue"
      iex> CubeGame.get_cubes(round)
      %{"blue" => 3, "green" => 9, "red" => 9}
  """
  def get_cubes(round) do
    cubes = String.split(round, ",") |> Enum.map(&String.trim/1)

    cubes_map = cubes
      |> Enum.reduce(%{}, fn cube, acc ->
        [count_str, color] = String.split(cube, " ")
        count = String.to_integer(count_str)
        Map.put(acc, color, count)
    end)
  end

  @doc"""
  Return bool given round map

  Check each key-value if exceeding max_cubes

  ## Example

      iex> round = %{red: 7, green: 12, blue: 16}
      iex> max_cubes = %{red: 12, green: 13, blue: 14}
      iex> CubeGame.is_possible_round(round)
      false
  """
  def is_possible_round(round) do
    max_cubes = %{"red" => 12, "green" => 13, "blue" => 14}

    result = Enum.all?(round, fn {color, count} ->
      Map.get(max_cubes, color, 0) >= count
    end)
  end

  def value_of_game(line) do
    valid_game = CubeGame.get_rounds(line)
      |> Enum.map(&CubeGame.get_cubes/1)
      |> Enum.map(&CubeGame.is_possible_round/1)
      |> Enum.all?()
    case valid_game do
      true ->
        CubeGame.get_game_id(line)
      false ->
        0
    end
  end
end


result =
  File.stream!("input-data.txt")
  |> Stream.map(&String.trim/1)
  |> Stream.map(&CubeGame.value_of_game/1)
  |> Enum.reduce(0, &(&1 + &2))

IO.puts("Total result: #{result}")
